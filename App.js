import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import CookieConsent from 'react-cookie-consent';

import Navbar from './components/Navbar';
import VideoModal from './components/VideoModal';
import ScienceVideo from './components/ScienceVideo';
import NoSweat from './components/NoSweat';

import Home from './screens/Home';
import Science from './screens/Science';
import Workouts from './screens/Workouts';
import Contact from './screens/Contact';
import Success from './screens/Success';
import NewsList from './screens/News/List';
import NewsShow from './screens/News/Show';
import Page from './screens/Page';
import Testimonials from './screens/Testimonials';
import ResearchPapers from './screens/ResearchPapers';

import './assets/css/bootstrap.css';
import './assets/css/bootstrap-theme.min.css';
import './assets/css/animate.min.css';
import './assets/css/fullpage.css';
import './assets/css/slick.css';
import './assets/css/slick-theme.css';
import './assets/css/style.css';

class App extends Component {

  constructor() {
    super();

    this.state = {loading: true};
  }

  render() {
    return (
      <React.Fragment>
        <div id="modal-ripple-animation"></div>

        <CookieConsent
          buttonText="Okay, thanks"
        >
          This site uses cookies: <a className="cookie-consent-link" href="/page/cookie-policy">Find out more.</a>
        </CookieConsent>

        <Navbar/>

        <VideoModal/>
        <ScienceVideo/>
        <NoSweat/>

        <Router>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/welcome" component={Home}/>
            <Route exact path="/science" component={Science}/>
            <Route exact path="/workouts" component={Workouts}/>
            <Route exact path="/contact" component={Contact}/>
            <Route exact path="/thank-you" component={Success}/>
            <Route exact path="/news" component={NewsList}/>
            <Route exact path="/news/:slug" component={NewsShow}/>
            <Route exact path="/page/:slug" component={Page}/>
            <Route exact path="/testimonials" component={Testimonials}/>
            <Route exact path="/research-papers" component={ResearchPapers}/>
          </Switch>
        </Router>

      </React.Fragment>
    );
  }
}

export default App;
