import React, {Component} from 'react';
import Footer from '../components/Footer';
import LiveDataManager from '../services/LiveDataManager';
import Config from '../config';
import {Form} from 'react-advanced-form';
import Meta from '../components/Meta';
import Modal from 'react-responsive-modal';
import moment from 'moment';

// Validation related packages
import isEmail from 'validator/lib/isEmail';
import CustomTextarea from '../components/form/CustomTextarea';
import CustomInput from '../components/form/CustomInput';
import CustomSelect from '../components/form/CustomSelect';
import CustomDatePicker from '../components/form/CustomDatePicker';

/**
 * Validation rules for the form.
 */
const customRules = {
  type: {
    email: ({value}) => isEmail(value)
  },
  name: {
    name: {
      minLength: ({value}) => (value.length > 0)
    },
    message: {
      maxLength: ({value}) => (value.length < 2500),
    }
  }
};

/**
 * Validation messages for the form.
 */
const customMessages = {
  name: {
    name: {
      missing: 'The name field is required!',
    },
    showroomName: {
      missing: 'The name field is required!',
    },
    message: {
      missing: 'The message field is required!',
      maxLength: 'Your message has to be less than 2500 characters!',
    },
    showroomMessage: {
      missing: 'The message field is required!',
      maxLength: 'Your message has to be less than 2500 characters!',
    },
    showroomDate: {
      missing: 'The date field is required!',
    },
    showroomPhone: {
      missing: 'The phone field is required!',
    },
  },
  type: {
    email: {
      missing: 'The email field is required!',
      invalid: 'The email must match a valid pattern!',
    },
  }
};

class Contact extends Component {
  handleSubmit = ({serialized, fields, form}) => {
    let emailContentPayload = {};

    switch (form.props.name) {
      case 'contacts':
        emailContentPayload = {
          fromName: serialized.name || '',
          subject: serialized.subject || '',
          body: serialized.message || '',
          replyTo: serialized.email || '',
          phone: serialized.phone || '',
        };
        break;
      case 'showroom-place':
        emailContentPayload = {
          fromName: serialized.showroomName || '',
          body: serialized.showroomMessage || '',
          replyTo: serialized.showroomEmail || '',
          showroomDate: moment(serialized.showroomDate).format('YYYY-MM-DD') || '',
          showroomPlace: this.state.showroomPlace || '',
          showroomPhone: serialized.showroomPhone || ''
        };
        break;
      default: break;
    }

    let optionsForSendCrm = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: JSON.stringify(emailContentPayload),
    };

    return fetch(Config.sendToCrm, optionsForSendCrm)
      .then((responseCrm) => {
        if (responseCrm.status === 200) {
          this.props.history.push('/thank-you');
        }
      })
      .catch((error) => console.log(error));
  };

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      contactForm: [],
      title: '',
      content: '',
      selectValue: '',
      meta: [],
      metatitle: [],
      showHackd: false,
      modalIsOpen: false,
      showroomPlace: ''
    };
  }

  componentDidMount() {
    if (window.location.hash === "#corporate") {
      this.setState({selectValue: 'Commercial client'});
    }
  }

  componentWillMount() {
    LiveDataManager
      .getPage('contact-us')
      .then((response) => this.setState((prevState, props) => {
        return {
          contactForm: response[0].acf.contact_form,
          title: response[0].acf.text_1,
          content: response[0].acf.text_2,
          whoAreYouOptions: response[0].acf.contact_form.who_you_are_options,
          loading: false,
          meta: response[0].yoast,
          metatitle: response[0].title.rendered,
        }
      }));
  }

  handleSubject(e) {
    this.setState({selectValue: e.nextValue});
  }

  onClickPlacesHandler = (e) => {
    if (e && e.target && e.target.href) {
      const { href, innerHTML } = e.target;
      if (href.indexOf('tel:') === -1 && href.indexOf('mailto:carol@carolfitai.com') === -1){
        e.preventDefault();
        this.showForm(innerHTML.replace(/&nbsp;/g, ' '));
      }
    }
  };

  showForm = showroomPlace => {
    this.setState({ showroomPlace });
    this.onOpenModal();
  };

  onOpenModal = () => this.setState({ modalIsOpen: true });

  onCloseModal = () => this.setState({ modalIsOpen: false });

  render() {

    const {
      contactForm, title, content, loading, whoAreYouOptions, selectValue, modalIsOpen,
      showroomPlace
    } = this.state;

    if (!loading) {

      return (
        <div className="wrapper">
          <Meta meta={this.state.meta.metadesc} metatitle={this.state.metatitle}/>
          <div className="contact">
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <h1>{title}</h1>

                  <p dangerouslySetInnerHTML={{__html: content}} onClick={this.onClickPlacesHandler} />
                </div>

                <div className="col-md-6">
                  <Form rules={customRules} name="contacts" messages={customMessages} action={this.handleSubmit}>
                    <div className="form-group">
                      <CustomInput name="name" type="text" placeholder={contactForm.placeholder_1} required/>
                    </div>

                    <div className="form-group">
                      <CustomInput name="email" type="email" placeholder={contactForm.placeholder_2} required/>
                    </div>

                    <div className="form-group">
                      <CustomInput name="phone" type="text" placeholder="Phone" />
                    </div>

                    <div className="form-group">
                      <CustomSelect className="form-control" onChange={(e) => this.handleSubject(e)} value={selectValue} id="subjectSelect" name="subject" required>
                        <option value="" disabled>{contactForm.placeholder_3}</option>
                        {whoAreYouOptions.map((option, index) => {
                          return (
                            <option key={'option' + index} value={option.option_title}>{option.option_title}</option>);
                        })}
                      </CustomSelect>
                    </div>

                    <div className="form-group">
                      <CustomTextarea name="message" className="form-control" required></CustomTextarea>
                    </div>

                    <button type="submit" className="btn btn-primary">{contactForm.button_text}</button>
                  </Form>
                </div>
              </div>
            </div>
          </div>
          <Modal open={modalIsOpen} onClose={this.onCloseModal} classNames={{ modal: 'showroom-modal-form' }} center>
            <Form rules={customRules} name="showroom-place" messages={customMessages} action={this.handleSubmit}>
              <div className="form-group">
                <CustomInput name="showroomName" type="text" placeholder={contactForm.placeholder_1} required/>
              </div>

              <div className="form-group">
                <CustomInput name="showroomEmail" type="email" placeholder={contactForm.placeholder_2} required/>
              </div>

              <div className="form-group">
                <CustomInput name="showroomPhone" type="text" placeholder="Phone" required />
              </div>

              <p className="place">Showroom: {showroomPlace}</p>

              <div className="form-group date-picker-field">
                <CustomDatePicker disablePast format="YYYY.MM.DD" name="showroomDate" required />
              </div>

              <div className="form-group">
                <CustomTextarea name="showroomMessage" className="form-control" />
              </div>

              <button type="submit" className="btn btn-primary">{contactForm.button_text}</button>
            </Form>
          </Modal>
          <Footer/>
        </div>

      );
    } else {
      return (<div></div>)
    }
  }
}

export default Contact;
